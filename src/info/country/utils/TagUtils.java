




package info.country.utils;





public class TagUtils {

    public static final String TAG_CAPITAL           = "capital";
    public static final String TAG_CURRENCY          = "currencyCode";
    public static final String TAG_COUNTRY_CODE      = "countryCode";
    public static final String TAG_LANGUAGES         = "languages";
    public static final String TAG_POPULATION        = "population";
    public static final String TAG_COUNTRY_NAME      = "countryName";
    public static final String TAG_GEONAMES          = "geonames";
    public static final String TAG_FLAG              = "flag";
    public static final String TAG_LATITUDE          = "lat";
    public static final String TAG_LONGITUDE         = "lng";
    public static final String TAG_WEATHER_CONDITION = "temperature_string";
    public static final String TAG_CLOUDS            = "icon_url";
    public static final String TAG_WEATHER_OBS       = "current_observation";

}
