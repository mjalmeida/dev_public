




package info.country;





import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;





public class CountryInfoServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;





    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
                    IOException {

        try {
            response.setContentType("application/json");
            IOUtils.copy(WebServiceClient.collectInfo(request.getParameter("countryName"), request.getParameter("code")),
                         response.getOutputStream());
        } catch (Exception e) {

            throw new ServletException();
        }
    }

}
