




package info.country;





import info.country.utils.TagUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Currency;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;





/**
 * 
 * Class responsible for collecting information regarding a given country. The
 * collected information is parsed and the information that will be displayed to
 * the user is sent, discarding the remaining info.
 * 
 * @author mjalmeida
 * 
 */
public class WebServiceClient {

    private final static String ENCODING            = "UTF-8";

    public final static String  URL_COUNTRY_INFO    = "http://api.geonames.org/countryInfoJSON?formatted=true&lang=en&country={0}&username=mjalmeida&style=full";
    public final static String  URL_CAPITAL_INFO    = "http://api.geonames.org/searchJSON?country={0}&maxRows=1&featureCode=PPLC&username=mjalmeida";
    public final static String  URL_CAPITAL_WEATHER = "http://api.wunderground.com/api/11a2bf6bd3f0b781/conditions/q/{0},{1}.json";





    /**
     * Collects all information provided by the different used web services and
     * sends to the client the relevant information that will be displayed.
     * Information is fetched and sent to the client in <code>JSON</code>
     * format.
     * 
     * 
     * @param countryName
     *            The country name
     * @param code
     *            The country code (ISO-3166)
     * @return An <code>InputStream</code> from which the collected information
     *         will be read.
     * @throws Exception
     *             If an error occurs while collecting information regarding the
     *             country.
     */
    public static InputStream collectInfo(String countryName, String code) throws Exception {

        ConcurrentMap<String, String> coutryInfo = new ConcurrentHashMap<String, String>();
        ExecutorService executersService = Executors.newCachedThreadPool();

        Thread countryInfoThread = getCountryInfoThread(coutryInfo, executersService, code);
        Thread weatherInfoThread = getCurrentWeatherThread(coutryInfo, executersService, code);

        executersService.execute(countryInfoThread);
        executersService.execute(weatherInfoThread);

        executersService.shutdown();

        while (!executersService.isTerminated()) {
        }

        return new ByteArrayInputStream(new JSONObject(coutryInfo).toString().getBytes(ENCODING));
    }





    /**
     * 
     * Auxiliary method that creates a thread responsible for fetching
     * information regarding the country. This information is provided by
     * invoking a json webservice provided by <a
     * href="http://www.geonames.org">http://www.geonames.org</a>.
     * 
     * @param coutryInfo
     *            <code>HashMap</code> where the fetched information that will
     *            be displayed to the user will be stored
     * @param executersService
     *            <code>Executer</code> responsible for executing this task
     * @param countryCode
     *            The country code (ISO-3166)
     * @return
     * @throws Exception
     */
    public static Thread getCountryInfoThread(final ConcurrentMap<String, String> coutryInfo,
                                              final ExecutorService executersService, final String countryCode)
                    throws Exception {

        return new Thread(new Runnable() {

            @Override
            public void run() {

                try {
                    parseCountryInfo(coutryInfo, getInputStream(getUrl(URL_COUNTRY_INFO, new String[] { countryCode })));
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }





    private static void parseCountryInfo(final ConcurrentMap<String, String> coutryInfo, InputStream is)
                    throws IOException, JSONException {

        StringWriter writer = new StringWriter();
        IOUtils.copy(is, writer, ENCODING);
        String theString = writer.toString();
        JSONObject jsonObj = (new JSONArray((new JSONObject(theString)).getString(TagUtils.TAG_GEONAMES))).getJSONObject(0);
        coutryInfo.putIfAbsent(TagUtils.TAG_CAPITAL, jsonObj.getString(TagUtils.TAG_CAPITAL));
        coutryInfo.putIfAbsent(TagUtils.TAG_CURRENCY,
                               Currency.getInstance(jsonObj.getString(TagUtils.TAG_CURRENCY)).getSymbol());
        coutryInfo.putIfAbsent(TagUtils.TAG_LANGUAGES, jsonObj.getString(TagUtils.TAG_LANGUAGES));
        coutryInfo.putIfAbsent(TagUtils.TAG_POPULATION, jsonObj.getString(TagUtils.TAG_POPULATION));
        coutryInfo.putIfAbsent(TagUtils.TAG_COUNTRY_NAME, jsonObj.getString(TagUtils.TAG_COUNTRY_NAME));
        coutryInfo.putIfAbsent(TagUtils.TAG_FLAG,
                               new MessageFormat("http://www.geonames.org/flags/m/{0}.png").format(new String[] { jsonObj.getString(TagUtils.TAG_COUNTRY_CODE).toLowerCase() }));
    }





    /**
     * 
     * Auxiliary method that creates a thread responsible for fetching
     * information regarding the country's capital current weather. This
     * information is provided by invoking a json webservice provided by <a
     * href="http://www.wunderground.com/">http://www.wunderground.com/</a>.
     * 
     * @param coutryInfo
     *            <code>HashMap</code> where the fetched information that will
     *            be displayed to the user will be stored
     * @param executersService
     *            <code>Executer</code> responsible for executing this task
     * @param countryCode
     *            The country code (ISO-3166)
     * @return
     * @throws Exception
     */
    public static Thread getCurrentWeatherThread(final ConcurrentMap<String, String> coutryInfo,
                                                 final ExecutorService executersService, final String code)
                    throws Exception {

        return new Thread(new Runnable() {

            @Override
            public void run() {

                try {
                    parseCapitalInfo(coutryInfo, getInputStream(getUrl(URL_CAPITAL_INFO, new String[] { code })));

                    if (coutryInfo.get(TagUtils.TAG_LATITUDE) != null && coutryInfo.get(TagUtils.TAG_LONGITUDE) != null) {
                        parseCapitalCurrentWeather(coutryInfo,
                                                   getInputStream(getUrl(URL_CAPITAL_WEATHER,
                                                                         new String[] {
                                                                                         coutryInfo.get(TagUtils.TAG_LATITUDE),
                                                                                         coutryInfo.get(TagUtils.TAG_LONGITUDE) })));
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }





    private static void parseCapitalInfo(final ConcurrentMap<String, String> coutryInfo, InputStream is)
                    throws IOException, JSONException {

        StringWriter writer = new StringWriter();
        IOUtils.copy(is, writer, ENCODING);
        String theString = writer.toString();
        JSONObject result = new JSONObject(theString);
        if (result.getLong("totalResultsCount") > 0) {
            JSONObject jsonObj = (new JSONArray((result).getString(TagUtils.TAG_GEONAMES))).getJSONObject(0);
            coutryInfo.putIfAbsent(TagUtils.TAG_LATITUDE, jsonObj.getString(TagUtils.TAG_LATITUDE));
            coutryInfo.putIfAbsent(TagUtils.TAG_LONGITUDE, jsonObj.getString(TagUtils.TAG_LONGITUDE));
        }
    }





    private static void parseCapitalCurrentWeather(final ConcurrentMap<String, String> coutryInfo, InputStream is)
                    throws IOException, JSONException {

        StringWriter writer = new StringWriter();
        IOUtils.copy(is, writer, ENCODING);
        String theString = writer.toString();
        JSONObject jsonObj = new JSONObject(theString).getJSONObject(TagUtils.TAG_WEATHER_OBS);
        coutryInfo.putIfAbsent(TagUtils.TAG_WEATHER_CONDITION, jsonObj.getString(TagUtils.TAG_WEATHER_CONDITION));
        coutryInfo.putIfAbsent(TagUtils.TAG_CLOUDS, jsonObj.getString(TagUtils.TAG_CLOUDS));
    }





    private static String getUrl(String base, String[] parameters) throws UnsupportedEncodingException {

        return new MessageFormat(base).format(parameters);
    }





    private static InputStream getInputStream(String url) throws Exception {

        return new URL(url).openStream();
    }
}
